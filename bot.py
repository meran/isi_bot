import telebot
import os

from flask import Flask, request
from flask_pymongo import PyMongo
import json
from pymongo import MongoClient

TOKEN = '1009047201:AAFuUB4wnMIoVAocH-ftah87o2jeoY7aqEM'

app = Flask(__name__)
# #app.config["MONGO_URI"] = "mongodb://localhost:27017/db_isi"
# app.config["MONGO_URI"] = "mongodb://root:root@amrazon-web-ivvcr.mongodb.com:27707/db_isi"
# mongo = PyMongo(app)

# tele_bot = mongo.db.tele_bot

client = MongoClient("mongodb://root:root@amrazon-web-shard-00-00-ivvcr.mongodb.net:27017,amrazon-web-shard-00-01-ivvcr.mongodb.net:27017,amrazon-web-shard-00-02-ivvcr.mongodb.net:27017/test?ssl=true&replicaSet=Amrazon-web-shard-0&authSource=admin&retryWrites=true&w=majority")

db = client.db_isi
tele_bot = db.tele_bot

bot = telebot.TeleBot(TOKEN)

@bot.message_handler(commands=['lihatdata'])
def data(message):
    output = []
    for q in tele_bot.find():
        output.append({'_id' : q['_id'], 'NAMA_PROJ' : q['NAMA_PROJ'], 'TGL_AKTIF_SISTEM' : q['TGL_AKTIF_SISTEM'], 'DESKRIPSI' : q['DESKRIPSI']})
    
    data_dumps = json.dumps(output)
    data_loads = json.loads(data_dumps)

    for w in data_loads:
        bot.send_message(message.chat.id, w['NAMA_PROJ']+' '+w['TGL_AKTIF_SISTEM'])
        

@bot.message_handler(commands=['start', 'help'])
def command_start(message):
    bot.send_message(message.chat.id, 
                     '<b>Supported commands ISI Bot Telgram : </b>\n'+
                     '/start or /help \n'+
                     '/lihatdata',parse_mode='HTML')

@bot.message_handler(func=lambda message: True)
def echo_all(message):
	bot.reply_to(message, text="Sorry, I didn't understand that command!\nUse /help to see support commands")
    
@app.route('/' + TOKEN,methods=['POST'])
def getMessage():
    bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode('utf-8'))])
    return 'ok webhook sudah terpasang !', 200

@app.route('/')
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url='https://isi-bot.herokuapp.com/'+TOKEN)
    return 'ok webhook sudah terpasang !', 200

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=int(os.environ.get('PORT',5000)))